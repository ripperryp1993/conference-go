from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def find_picture(search_item):
    pexels_url = "https://api.pexels.com/v1/search?query=" + search_item
    header = {"Authorization": PEXELS_API_KEY}

    r = requests.get(pexels_url, headers=header)
    r = json.loads(r.content)
    photos = r["photos"]
    if photos and len(photos) > 0:
        return photos


def get_weather(city, state):
    geocode_url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ","
        + "US&limit=5&appid="
        + OPEN_WEATHER_API_KEY
    )
    response = requests.get(geocode_url)
    data = json.loads(response.content)
    lon, lat = data["lon"], data["lat"]

    weather_url = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + str(lat)
        + "&lon="
        + str(lon)
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )
    res_weather = requests.get(weather_url)
    weather_data = json.loads(res_weather.content)
    return weather_data
